const bd =[
    {"Id":0,"Apellido":"Baque", "Nombre":"Xavier", "Semestre": "Cuarto", 
    "Paralelo":"A", "Direccion":"Manta", "Telefono": "0934334344",
    "Correo":"xbaque@gmail.com"},
    {"Id":1,"Apellido":"Iñiguez", "Nombre":"Marcos", "Semestre": "Quinto", 
    "Paralelo":"B", "Direccion":"Manta", "Telefono": "09854544532",
    "Correo":"maiñiguez@gmail.com"},
    {"Id":2,"Apellido":"Vera", "Nombre":"Gema", "Semestre": "Quinto", 
    "Paralelo":"A", "Direccion":"Manta", "Telefono": "0987654323",
    "Correo":"gvera@gmail.com"},
    {"Id":3,"Apellido":"Medranda", "Nombre":"Isaac", "Semestre": "Quinto", 
    "Paralelo":"A", "Direccion":"Manta", "Telefono": "0954543434",
    "Correo":"imedranda@gmail.com"},
    {"Id":4,"Apellido":"Rivera", "Nombre":"Nestor", "Semestre": "Tercero", 
    "Paralelo":"B", "Direccion":"Manta", "Telefono": "09943993934",
    "Correo":"nrivera@gmail.com"},
    {"Id":5,"Apellido":"Soledispa", "Nombre":"Bruce", "Semestre": "Cuarto", 
    "Paralelo":"C", "Direccion":"Manta", "Telefono": "0987654311",
    "Correo":"bsoledispa@gmail.com"},
    {"Id":6,"Apellido":"Vera", "Nombre":"Maria Belen", "Semestre": "Quinto", 
    "Paralelo":"B", "Direccion":"Manta", "Telefono": "0985655451",
    "Correo":"mbelen@gmail.com"},
    {"Id":7,"Apellido":"Zambrano", "Nombre":"Luis", "Semestre": "Quinto", 
    "Paralelo":"A", "Direccion":"San Mateo", "Telefono": "0986565344",
    "Correo":"lzambrano@gmail.com"},
    {"Id":8,"Apellido":"Macias", "Nombre":"Kevin", "Semestre": "Cuarto", 
    "Paralelo":"A", "Direccion":"Manta", "Telefono": "09545454554",
    "Correo":"kmacias@gmail.com"},
    {"Id":9,"Apellido":"Mendoza", "Nombre":"Johan", "Semestre": "Quinto", 
    "Paralelo":"A", "Direccion":"Manta", "Telefono": "0965665344",
    "Correo":"jmendoza@gmail.com"}
]

const estudiantes = document.querySelectorAll('.nombre_estudiante');

estudiantes.forEach((estudiante)=>{
    estudiante.addEventListener('click', (nombre)=>{
        let id=nombre.target.getAttribute('estu-id');
        bd.forEach((estudiante)=>{
            if(id == estudiante.Id){
                const verDetalle=nombre.target.parentElement.lastElementChild;
                verDetalle.innerHTML=`
                                    <div class="lista">

                                        <div class="nom">
                                        <h2>Nombre:</h2>
                                        <p>${estudiante.Nombre}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Apellido:</h2>
                                            <p>${estudiante.Apellido}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Correo:</h2>
                                            <p>${estudiante.Correo}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Telefono:</h2>
                                            <p>${estudiante.Telefono}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Direccion:</h2>
                                            <p>${estudiante.Direccion}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Semestre:</h2>
                                            <p>${estudiante.Semestre}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Paralelo:</h2>
                                            <p>${estudiante.Paralelo}</p>
                                        </div> 
                                    </div>`

            }
        })
    })
})

