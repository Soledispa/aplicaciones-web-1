function ejercicio1() {
    let date = new Date();
    document.getElementById("entrada1").value =
      date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    document.getElementById("salida1").value =
      date.getHours() * 3600 +
      date.getMinutes() * 60 +
      date.getSeconds() +
      " s";
  }
  
  function ejercicio2() {
    let base = document.getElementById("entrada2").value;
    let altura = document.getElementById("entrada2-1").value;
    document.getElementById("salida2").value =
      (base * altura) / 2;
  }

  function ejercicio3() {
    let num = document.getElementById("entrada3").value;
    let raiz = Math.sqrt(num);
    let result;
    if (raiz < 10) {
      result = raiz.toFixed(2);
    } else {
      if (raiz < 100) {
        result = raiz.toFixed(1);
      } else {
        result = raiz.toFixed(0);
      }
    }
    document.getElementById("salida3").value =
        " es: " + result;
  }

  function ejercicio4() {
    let cadena = document.getElementById("entrada4").value;
    document.getElementById("salida4").value =
      " ingresada es de: " +
      cadena.length +
      " caracteres";
  }

